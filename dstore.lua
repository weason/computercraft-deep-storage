print_debug = true

pos_offset = {
    x = 0,
    y = 0,
    z = 0
}


-- geometry

Geom = {
    rows = 5,
    cols = 5,
    floors = 2
}

-- Utility functions

function d_print(msg)
    if print_debug then print(msg) end
end

function sleep (n)
    d_print("sleep: " .. n)
    local sec = tonumber(os.clock() + n);
    while (os.clock() < sec) do
    end
end


--- Get floor z position.
-- Calculates the z position of the given floor.
-- The home position is floor 0, and the first storage floor is floor 1.
-- @param floor the floor
function Geom.floor_z(floor)
    return 3 * floor
end


-- movement

Move = {}

function Move.up()
    result = turtle.up()

    if result then
        pos_offset.z = pos_offset.z + 1
    end

    return result
end

function Move.down()
    result = turtle.down()

    if result then
        pos_offset.z = pos_offset.z - 1
    end

    return result
end

function Move.front()
    result = turtle.front()

    if result then
        pos_offset.y = pos_offset.y + 1
    end

    return result
end

function Move.back()
    result = turtle.back()

    if result then
        pos_offset.y = pos_offset.y - 1
    end

    return result
end

function Move.right()
    -- TODO
    turtle.turnRight()
    result = turtle.forward()
    turtle.turnLeft()

    if result then
        pos_offset.x = pos_offset.x + 1
    end

    return result
end

function Move.left()
    -- TODO
    turtle.turnLeft()
    result = turtle.forward()
    turtle.turnRight()

    if result then
        pos.offset_x = pos_offset.x - 1
    end

    return result
end


-- navigation

Nav = {}

function Nav.go_to_x(x)
    dx = x - pos_offset.x
    d_print("Go to x " .. x .. ": cur x = " .. pos_offset.x .. ", dx = " .. dx)
    if dx == 0 then return true end

    if dx < 0 then
        fn = Move.left
    elseif dx > 0 then
        fn = Move.right
    end

    for i=1,math.abs(dx) do
        result = fn()
        if not result then return false end
    end

    return true
end

function Nav.go_to_y(y)
    dy = y - pos_offset.y
    d_print("Go to y " .. y .. ": cur y = " .. pos_offset.y .. ", dy = " .. dy)
    if dy == 0 then return true end

    if dy < 0 then
        fn = Move.back
    elseif dy > 0 then
        fn = Move.front
    end

    for i=1,math.abs(dy) do
        result = fn()
        if not result then return false end
    end

    return true
end

function Nav.go_to_z(z)
    dz = (floor_z - pos_offset.z)
    d_print("Go to z " .. z .. ": cur z = " .. pos_offset.z .. ", dz = " .. dz)
    if dz == 0 then return true end

    if dz < 0 then
        fn = Move.down
    elseif dz > 0 then
        fn = Move.up
    end

    for i=1,math.abs(dz) do
        result = fn()
        if not result then return false end
    end

    return true
end


function Nav.go_to(x, y, floor)
    if not Nav.go_to_floor(floor) then return false end

    if not Nav.go_to_x(x) then return false end
    if not Nav.go_to_y(y) then return false end

    return true
end


function Nav.go_to_floor(floor)
    floor_z = Geom.floor_z(floor)
    d_print("Go to floor " .. floor .. ": cur z = " .. pos_offset.z .. ", dst z = " .. floor_z)

    if pos_offset.z ~= floor_z then
        if not Nav.go_to_x(0) then return false end
        if not Nav.go_to_y(0) then return false end
        if not Nav.go_to_z(floor_z) then return false end
    end

    return true
end


-- ###########################################################
-- Entry point

sleep(5)

Nav.go_to(2, 2, -1)
sleep(1)
Nav.go_to(-2, -2, -1)
sleep(1)
Nav.go_to(-2, -2, -2)
sleep(1)
Nav.go_to(0, 0, 0)
