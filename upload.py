"""
## Links
- https://pypi.org/project/Pastebin/
- https://pastebin.com/doc_api#2
"""

import dotenv
import os
from datetime import datetime
import requests
import logging


dotenv.load_dotenv()


def upload(api_dev_key: str, text: str, paste_name: str, paste_format: str):
    """Upload the text to pastebin"""
    url = 'https://pastebin.com/api/api_post.php'
    data = {
        "api_option": "paste",
        "api_paste_expire_date": "1D",
        "api_dev_key": api_dev_key,
        "api_paste_name": os.path.basename(upload_file),
        "api_paste_format": paste_format,
        "api_paste_code": text
    }

    r = requests.post(url, data)
    print(r.status_code)
    print(r.text)

    return r.text.split("/")[-1]


if __name__ == '__main__':
    api_dev_key = os.getenv("PASTEBIN_KEY")
    upload_file = "dstore.lua"
    # upload_file = "test_chests.lua"

    with open(upload_file, "r") as paste_file:
        text = paste_file.read()

    paste_id = upload(api_dev_key, text, os.path.basename(upload_file), "lua")

    print("To download on a turtle, run this command:")
    print(f"  pastebin get {paste_id} run.lua")
